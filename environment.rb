require 'data_mapper'
require 'sinatra/base'
require 'json'

DATABASE = ENV['DATABASE_URL'] || "sqlite://" + File.join(File.expand_path("../data", __FILE__), "finance.sqlite")

puts "Database: " + DATABASE

#DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, DATABASE)

require './lib/models'

DataMapper.finalize
DataMapper.auto_upgrade!

require './lib/sharkbait'
