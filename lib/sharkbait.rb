class SharkBait < Sinatra::Base

  before do
    content_type :json
  end

  helpers do
    def expand_contributions(contributions)
      con_rcvd = []
      contributions.each do |contribution|
        con = JSON.parse(contribution.to_json)
        con['contributor'] = contribution.contributor
        con['cash'] = con['cash'] ? con['cash'].to_f : 0
        con_rcvd << con
      end
      return con_rcvd
    end

  end

  get '/' do
    "Hello World"
  end

  get '/candidates/all' do
    response = []
    Candidate.all.each do |c|
      if(c.first_name && c.last_name)
        response.push({:name => c.first_name + " " + c.last_name, :id => c.candidate_id})
      end
    end
    return response.to_json
  end

  get '/candidates/:id' do
    candidate = Candidate.first(:candidate_id => params[:id])
    if(candidate)
      response = JSON.parse(candidate.to_json)
      response['cash_received'] = candidate.cash_contribution_totals
      response['contributions_received'] = expand_contributions(candidate.contributions_received)
      response['other_committees'] = candidate.other_committees
      response['campaigns'] = candidate.campaigns
      response.to_json
    else
      nil.to_json
    end
  end

  get '/contributors/export' do
    Contribution.all.to_json
  end

  get '/contributors/:id' do
    Contributor.first(:contributor_id => params[:id]).to_json
  end

end
