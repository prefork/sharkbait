class FilingEvent
  include DataMapper::Resource

  property :id, Serial
  property :type, String
  property :office_code, String
  property :office_held, String
  property :office_area, String
  property :filing_date, Date

  belongs_to :candidate
end

class Candidate
  include DataMapper::Resource

  property :id, Serial
  property :candidate_id, String, :index => true
  property :first_name, String
  property :middle_initial, String
  property :last_name, String

  has n, :filing_events
  has n, :campaigns
  has n, :committees, :through => :campaigns

  def candidate_committee
    committees.first(:type => 'candidate_committee')
  end

  def other_committees
    committees.all(:type.not => 'candidate_committee')
  end

  def contributions_received
    if(candidate_committee)
      candidate_committee.contributions
    else
      return []
    end
  end

  def cash_contribution_totals
    in_state = 0
    out_of_state = 0
    contributions_received.each do |c|
      if(c.contributor.state == 'NE')
        in_state += c.cash.to_f
      else
        out_of_state += c.cash.to_f
      end
    end
    return {'out_of_state' => out_of_state, 'in_state' => in_state}
  end
end

class Campaign
  include DataMapper::Resource

  property :id, Serial
  property :office_sought, String
  property :office_title, String
  property :office_description, String
  property :status, String

  belongs_to :candidate
  belongs_to :committee
end

class Committee
  include DataMapper::Resource

  property :id, Serial
  property :committee_id, String, :index => true
  property :name, String
  property :zipcode, String
  property :city, String
  property :state, String
  property :type, String

  has n, :campaigns
  has n, :contributions
  has n, :candidates, :through => :campaigns
  has n, :contributors, :through => :contributions

end

class Contribution
  include DataMapper::Resource

  property :id, Serial
  property :date, Date
  property :cash, Decimal
  property :in_kind, Decimal
  property :unpaid_pledge, Decimal

  belongs_to :contributor
  belongs_to :committee
end

class Contributor
  include DataMapper::Resource

  property :id, Serial
  property :contributor_id, String, :index => true
  property :type, String
  property :first_name, String
  property :middle_initial, String
  property :last_name, String
  property :organization_name, String
  property :zipcode, String
  property :city, String
  property :state, String

  has n, :contributions
  has n, :committees, :through => :contributions
end


