require 'logger'

require_relative '../environment'

DATA_DIR = File.expand_path("../../data/csv", __FILE__)
LOGGER = Logger.new($stdout)

committee_type = {
  'C' => 'candidate_committee',
  'B' => 'ballot_question',
  'P' => 'political_action_committee',
  'T' => 'political_party_committee',
  'I' => 'independent_reporting_committee',
  'R' => 'independent_reporting_committee',
  'S' => 'separate_segregated_political_fund_committee'
}

LOGGER.info "Beginning migration"

CSV.read(File.join(DATA_DIR,"forma1.csv"), :headers => true).each do |row|
  committee_id = row['Committee ID Number']
  type = committee_type[row['Committee Type']] || 'unknown'

  LOGGER.info "Setting type for #{committee_id} to #{type}"
  committee = Committee.first(:committee_id => committee_id)
  committee.type = type
  committee.save
end

LOGGER.info "Done"
