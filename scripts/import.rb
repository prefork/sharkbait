require 'logger'

require_relative '../environment'

DATA_DIR = File.expand_path("../../data/csv", __FILE__)
LOGGER = Logger.new($stdout)

LOGGER.info "Starting import job"

LOGGER.info "(db) load committees"
CSV.read(File.join(DATA_DIR,"forma1.csv"), :headers => true).each do |row|
  committee_id = row['Committee ID Number']

  committee = Committee.first_or_create({ :committee_id => committee_id }, { 
    :name    => row['Committee Name'],
    :zipcode => row['Committee Zip'],
    :city    => row['Committee City'],
    :state   => row['Committee State']
  })

  committee.save
end

LOGGER.info "(db) load candidates and filing events"
CSV.read(File.join(DATA_DIR,"formc1.csv"), :headers => true).each do |row|
  candidate_id = row['Candidate ID']

  # get a record to work on
  candidate = Candidate.first_or_create({:candidate_id => candidate_id}, {
    :first_name    => row['Candidate First Name'],
    :middle_initial   => row['Candidate Middle Initial'],
    :last_name     => row['Candidate Last Name'],
    :candidate_id  => row['Candidate ID'],
    :filing_events => []
  })

  event_type = :unknown
  if(row['New Appt'])
    event_type = :newly_elected
  elsif(row['Elective Office'])
    event_type = :seeking_office
  elsif(row['Annual or Employee Report'])
    event_type = :annual_report
  end

  candidate.filing_events << FilingEvent.first_or_create({
    :office_code => row['Office Code'],
    :office_held => row['Office Held'],
    :office_area => row['Subdivision'],
    :type => event_type.to_s,
    :filing_date => DateTime.strptime(row['Date Received'], "%m/%d/%Y")
  })

  candidate.save
end

LOGGER.info "(db) load contributors"
contributor_types = {
  "I" => 'individual',
  "B" => 'business',
  "C" => 'corporation',
  "M" => 'candidate_committee',
  "P" => 'pac',
  "Q" => 'ballot_question',
  "R" => 'political_party',
  "U" => 'unknown'
}

CSV.read(File.join(DATA_DIR,"formb1ab.csv"), :headers => true).each do |row|
  contributor_id = row['Contributor ID']

  # get a record to work on
  contributor = Contributor.first_or_create({:contributor_id => contributor_id}, {
    :first_name        => row['Contributor First Name'],
    :middle_initial       => row['Contributor Middle Initial'],
    :last_name         => row['Contributor Last Name'],
    :organization_name => row['Contributor Organization Name'],
    :zipcode          => row['Contributor Zipcode'],
    :state             => row['Contributor State'],
    :city              => row['Contributor City'],
    :type              => contributor_types[row['Type of Contributor'] || "U"]
  })

  contributor.contributions << Contribution.first_or_create({
    :committee     => Committee.first(:committee_id => row['Committee ID']),
    :date          => DateTime.strptime(row['Contribution Date'], "%Y-%m-%d"),
    :cash          => row['Cash Contribution'],
    :in_kind       => row['In-Kind Contribution'],
    :unpaid_pledge => row['Unpaid Pledges']
  })

  contributor.save
end

LOGGER.info "(db) load campaigns"
CSV.read(File.join(DATA_DIR,"forma1cand.csv"), :headers => true).each do |row|
  campaign = Campaign.first_or_create({
    :status             => row['Support/Oppose'] == 1 ? :oppose : :support,
    :office_sought      => row['Office Sought'],
    :office_title       => row['Office Title'],
    :office_description => row['Office Description'],
    :candidate          => Candidate.first(:candidate_id => row['Candidate ID']),
    :committee          => Committee.first(:committee_id => row['Form A1 ID Number'])
  })
  campaign.save
end

LOGGER.info "(db) done!"
